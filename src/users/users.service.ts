import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { StreamerEntity } from 'src/streamers/streamers.entity';

const relations = [
    "client",
    "streamer"
]
@Injectable()
export class UsersService {
    constructor(
        @Inject('USERS_REPOSITORY')
        private userRepository: Repository<UserEntity>,
        private jwtService: JwtService
    ) { }

    async crateUser(user: UserEntity): Promise<UserEntity> {
        return this.userRepository.save(user);
    }

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.userRepository.findOne({
            where: {
                email: email
            }
        });
        
        if (user) {
            const isMatch = await bcrypt.compare(pass, user.password);
            if (isMatch) {
                const { password, ...result } = user;
                return result;
            }
        }
        return null;
    }


    async login(user: UserEntity) {
        const payload = { username: user.email, sub: user.id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async findOne(id: number): Promise<UserEntity> {
        return this.userRepository.findOne({ 
            where: { id: id },
            relations: [
                "client",
                "streamer"
            ]
        });
    }

    async findOneStreamer(id: number): Promise<UserEntity> {
        return this.userRepository.findOne({ 
            where: { id: id },
            relations: [
                "streamer"
            ]
        });
    }
}
