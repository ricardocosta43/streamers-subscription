import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UsersDto {
  @ApiProperty({ description: "Email do usuário" })
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({ description: "Senha do usuário" })
  @IsNotEmpty()
  @IsString()
  password: string;
}