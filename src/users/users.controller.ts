import { Controller, HttpStatus, Post, Res, UseGuards, Request, Get, Body } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { UsersService } from 'src/users/users.service';
import { LocalAuthGuard } from 'src/users/guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { UsersDto } from './dto/users-dto';

@Controller('users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService
    ) { }

    @UseGuards(LocalAuthGuard)
    @ApiTags('Authentication')
    @Post('auth/login')
    async login(
        @Request() req,
        @Body() body: UsersDto,
        @Res() response: Response
    ): Promise<any> {
        // const user = new UserEntity
        // user.email = body.username;
        // user.password = body.password;

        // console.log(req.user)

        const token = await this.usersService.login(req.user);
        const userResp = req.user;
        const responses = { token, userResp }
        response
            .status(HttpStatus.OK)
            .send({ responses, message: 'OK' });
    }

    @ApiTags('Authentication')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @Get('get-user')
    async getUser(
        @Request() req,
        @Res() response: Response
    ): Promise<any> {
        try {
            const user = await this.usersService.findOne(req.user.userId)

            if (user) {
                response
                    .status(HttpStatus.OK)
                    .send({ user, message: 'OK' });
            } else {
                response
                    .status(HttpStatus.BAD_REQUEST)
                    .send({ error: "Client not found", message: 'BAD REQUEST' });
            }
        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }
}
