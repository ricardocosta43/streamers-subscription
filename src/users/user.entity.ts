import { Exclude } from "class-transformer";
import { IsInt, IsString } from "class-validator";
import { ClientsEntity } from "src/clients/clients.entity";
import { StreamerEntity } from "src/streamers/streamers.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("users")
export class UserEntity {
    @PrimaryGeneratedColumn()
    @IsInt()
    id: number;

    @Column()
    @IsString()
    email: string;

    @Exclude()
    @Column()
    @IsString()
    password: string;

    @OneToOne(() => StreamerEntity, streamer => streamer.user)
    @JoinColumn({ name: "id" })
    streamer: StreamerEntity;

    @OneToOne(() => ClientsEntity, client => client.userId)
    @JoinColumn({ name: "id" })
    client: ClientsEntity;

}