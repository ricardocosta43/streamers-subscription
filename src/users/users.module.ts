import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { DatabaseModule } from 'src/database/database.module';
import { usersProviders } from './users.providers';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/shared/constants';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '86400s' }, // 24 HORAS
      //signOptions: { expiresIn: '120s' }, // 2 min
    }),
  ],
  providers: [
    ...usersProviders,
    UsersService,
    LocalStrategy,
    JwtStrategy,
  ],
  controllers: [UsersController],
  exports: [UsersService]
})
export class UsersModule { }
