import { Body, Controller, HttpStatus, Post, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/users/guards/jwt-auth.guard';
import { Md5 } from 'ts-md5';
import { DonationsEntity } from './donations.entity';
import { DonationsService } from './donations.service';
import { DonationCardDto, DonationPixDto } from './dto/donatios-dto';
import { TypeDonate } from './enums/typeDonate';

@Controller('donations')
export class DonationsController {
    constructor(
        private readonly donationsService: DonationsService,
    ) { }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @ApiTags('Donations')
    @Post('donation-pix')
    async donatioPix(
        @Body() body: DonationPixDto,
        @Res() response: Response
    ): Promise<any> {

        let donation = new DonationsEntity;
        donation.clientId = body.clientId
        donation.streamerId = body.streamerId
        donation.type = TypeDonate.PIX
        donation.pixKey = Md5.hashStr(body.pixKey)
        donation.value = body.value

        try {
            const result = await this.donationsService.createDonation(donation)
            response
                .status(HttpStatus.OK)
                .send({ data: result, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @ApiTags('Donations')
    @Post('donation-credit-card')
    async donatioCreditCard(
        @Body() body: DonationCardDto,
        @Res() response: Response
    ): Promise<any> {

        let donation = new DonationsEntity;
        donation.clientId = body.clientId
        donation.streamerId = body.streamerId
        donation.type = TypeDonate.CREDIT_CARD
        donation.creditCardHash = Md5.hashStr(body.cardNumber + body.validAt + body.cvv.toString())
        donation.value = body.value

        try {
            const result = await this.donationsService.createDonation(donation)
            response
                .status(HttpStatus.OK)
                .send({ data: result, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }
}
