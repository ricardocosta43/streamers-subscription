import { Connection } from "typeorm";
import { DonationsEntity } from "./donations.entity";

export const donationsProviders = [
    {
        provide: 'DONATIONS_REPOSITORY',
        useFactory: (connection: Connection) =>
            connection.getRepository(DonationsEntity),
        inject: ['DATABASE_CONNECTION'],
    },
];