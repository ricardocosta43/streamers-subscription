import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class DonationPixDto {
    @ApiProperty({ description: "Código do cliente" })
    @IsNotEmpty()
    @IsInt()
    clientId: number;

    @ApiProperty({ description: "Código do streamer" })
    @IsNotEmpty()
    @IsInt()
    streamerId: number;

    // @ApiProperty({ 
    //     description: "Tipo de transação",
    //     enum: [
    //         'PIX',
    //         'CREDIT_CARD',
    //     ]
    // })
    // @IsNotEmpty()
    // @IsString()
    // type: TypeDonate;

    @ApiProperty({ description: "Chave Pix" })
    @IsNotEmpty()
    @IsString()
    pixKey: string;

    @ApiProperty({ description: "Valor da doação" })
    @IsNotEmpty()
    @IsNumber()
    value: number;
}

export class DonationCardDto {
    @ApiProperty({ description: "Código do cliente" })
    @IsNotEmpty()
    @IsInt()
    clientId: number;

    @ApiProperty({ description: "Código do streamer" })
    @IsNotEmpty()
    @IsInt()
    streamerId: number;

    @ApiProperty({ description: "Número do cartão" })
    @IsNotEmpty()
    @IsString()
    cardNumber: string;

    @ApiProperty({ description: "Data de validade do cartão" })
    @IsNotEmpty()
    @IsString()
    validAt: string

    @ApiProperty({ description: "Código de segurança do cartão" })
    @IsNotEmpty()
    @IsInt()
    cvv: number

    @ApiProperty({ description: "Valor da doação" })
    @IsNotEmpty()
    @IsNumber()
    value: number;
}