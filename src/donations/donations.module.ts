import { Module } from '@nestjs/common';
import { DonationsService } from './donations.service';
import { DonationsController } from './donations.controller';
import { DatabaseModule } from 'src/database/database.module';
import { donationsProviders } from 'src/donations/donations.providers';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...donationsProviders,
    DonationsService
  ],
  controllers: [DonationsController],
  exports: [DonationsService]
})
export class DonationsModule { }
