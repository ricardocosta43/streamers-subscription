import { IsInt, IsNumber, IsString } from "class-validator";
import { ClientsEntity } from "src/clients/clients.entity";
import { StreamerEntity } from "src/streamers/streamers.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("donations")
export class DonationsEntity {
    @PrimaryGeneratedColumn()
    @IsInt()
    id: number;

    @Column({ name: "client_id" })
    @IsInt()
    clientId: number;

    @Column({ name: "streamer_id" })
    @IsInt()
    streamerId: number;

    @Column({ length: 20 })
    @IsString()
    type: string;

    @Column({ length: 255, name: "pix_key" })
    @IsString()
    pixKey: string;

    @Column({ length: 255, name: "credit_card_hash" })
    @IsString()
    creditCardHash: string;

    @Column({})
    @IsNumber()
    value: number;

    @ManyToOne(() => ClientsEntity, client => client.id)
    @JoinColumn({ name: "client_id" })
    client: ClientsEntity;

    @ManyToOne(() => StreamerEntity, streamer => streamer.id)
    @JoinColumn({ name: "streamer_id" })
    streamer: StreamerEntity;
}