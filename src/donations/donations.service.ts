import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { DonationsEntity } from './donations.entity';

const relations = [
    "client",
    "streamer"
]
@Injectable()
export class DonationsService {
    constructor(
        @Inject('DONATIONS_REPOSITORY')
        private donationsRepository: Repository<DonationsEntity>,
    ) { }

    async createDonation(donation: DonationsEntity): Promise<DonationsEntity> {
        return this.donationsRepository.save(donation);
    }

    async getDonationsStreamer(streamerId: number): Promise<DonationsEntity[]> {
        return this.donationsRepository.find({
            where:{
                streamerId: streamerId
            },
            relations: relations
        });
    }

    async getDonationsClient(clientId: number): Promise<DonationsEntity[]> {
        return this.donationsRepository.find({
            where:{
                clientId: clientId
            },
            relations: relations
        });
    }
}
