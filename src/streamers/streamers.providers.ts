import { Connection } from "typeorm";
import { StreamerEntity } from "./streamers.entity";

export const streamersProviders = [
    {
        provide: 'STREAMERS_REPOSITORY',
        useFactory: (connection: Connection) =>
            connection.getRepository(StreamerEntity),
        inject: ['DATABASE_CONNECTION'],
    },
];