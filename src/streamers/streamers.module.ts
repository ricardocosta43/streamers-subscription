import { Module } from '@nestjs/common';
import { StreamersService } from './streamers.service';
import { StreamersController } from './streamers.controller';
import { streamersProviders } from './streamers.providers';
import { DatabaseModule } from 'src/database/database.module';
import { UsersModule } from 'src/users/users.module';
import { DonationsModule } from 'src/donations/donations.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    DonationsModule,
  ],
  providers: [
    ...streamersProviders,
    StreamersService
  ],
  controllers: [StreamersController]
})
export class StreamersModule {}
