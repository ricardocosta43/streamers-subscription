import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class StreamerRegisterDto {
  @ApiProperty({ description: "Email" })
  @IsNotEmpty()
  @IsString()
  email: string;

  @ApiProperty({ description: "Senha" })
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiProperty({ description: "Nome do canal do Streamer" })
  @IsNotEmpty()
  @IsString()
  channelName: string;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  bank: number;

  @ApiProperty({ description: "Código da conta corrente" })
  @IsNotEmpty()
  @IsInt()
  cc: number;

  @ApiProperty({ description: "Dígito verificador da conta corrente" })
  @IsNotEmpty()
  @IsInt()
  dv_cc: number;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  agency: number;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  dv_agency: number;
}

export class StreamerDto {
  @ApiProperty({ description: "Id do Streamer" })
  @IsNotEmpty()
  @IsString()
  id: string;
}

export class StreamerUpdateDto {
  @ApiProperty({ description: "Id do Streamer" })
  @IsNotEmpty()
  @IsInt()
  id: number;

  @ApiProperty({ description: "Nome do canal do Streamer" })
  @IsNotEmpty()
  @IsString()
  channelName: string;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  bank: number;

  @ApiProperty({ description: "Código da conta corrente" })
  @IsNotEmpty()
  @IsInt()
  cc: number;

  @ApiProperty({ description: "Dígito verificador da conta corrente" })
  @IsNotEmpty()
  @IsInt()
  dvCc: number;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  agency: number;

  @ApiProperty({ description: "Código do banco que possui conta" })
  @IsNotEmpty()
  @IsInt()
  dvAgency: number;
}