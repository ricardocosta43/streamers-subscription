import { IsInt, IsString } from "class-validator";
import { DonationsEntity } from "src/donations/donations.entity";
import { UserEntity } from "src/users/user.entity";
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("streamers")
export class StreamerEntity {
    @PrimaryGeneratedColumn()
    @IsInt()
    id: number;

    @Column()
    @IsInt()
    bank: number;

    @Column()
    @IsInt()
    cc: number;

    @Column({ name: "dv_cc" })
    @IsInt()
    dvCc: number;

    @Column()
    @IsInt()
    agency: number;

    @Column({ name: "dv_agency" })
    @IsInt()
    dvAgency: number;

    @Column({ name: "user_id" })
    @IsInt()
    userId: number;

    @Column({ length: 255, name: "channel_name" })
    @IsString()
    channelName: string;

    @OneToMany(() => DonationsEntity, donation => donation.streamer)
    donations: DonationsEntity[];

    @OneToOne(() => UserEntity, user => user.streamer)
    @JoinColumn({ name: "user_id" })
    user: UserEntity;
}