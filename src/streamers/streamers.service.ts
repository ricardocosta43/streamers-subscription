import { Inject, Injectable } from '@nestjs/common';
import { DonationsEntity } from 'src/donations/donations.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { StreamerEntity } from './streamers.entity';

@Injectable()
export class StreamersService {
    constructor(
        @Inject('STREAMERS_REPOSITORY')
        private streamerRepository: Repository<StreamerEntity>,
    ) { }

    async createStreamer(streamer: StreamerEntity): Promise<StreamerEntity> {
        return this.streamerRepository.save(streamer);
    }

    async getOneStreamer(id: number): Promise<StreamerEntity> {
        return this.streamerRepository.findOne(id);
    }

    async getAllStreamers(): Promise<StreamerEntity[]> {
        return this.streamerRepository.find();
    }

    async updateStreamer(id: number, streamer: StreamerEntity): Promise<UpdateResult> {
        return this.streamerRepository.update(id, streamer);
    }

    async deleteStreamer(id: number): Promise<DeleteResult> {
        return this.streamerRepository.delete(id);
    }

    async getStreamer(userId: number): Promise<StreamerEntity> {
        return this.streamerRepository.findOne({
            where:{
                userId: userId
            },
            relations: [
                "user"
            ]
        });
    }
    
}
