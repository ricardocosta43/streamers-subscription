import { Body, Controller, HttpStatus, Post, Res, UseGuards, Request, Get, Query, Put, Delete } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { StreamersService } from './streamers.service';
import { Response } from 'express';
import { StreamerDto, StreamerRegisterDto, StreamerUpdateDto } from './dto/streamers-dto';
import { StreamerEntity } from './streamers.entity';
import { UsersService } from 'src/users/users.service';
import { UserEntity } from 'src/users/user.entity';
import * as bcrypt from 'bcrypt';
import { DonationsService } from 'src/donations/donations.service';
import { JwtAuthGuard } from 'src/users/guards/jwt-auth.guard';

@Controller('streamers')
export class StreamersController {
    constructor(
        private readonly streamersService: StreamersService,
        private readonly usersService: UsersService,
        private readonly donationsService: DonationsService
    ) { }

    @ApiTags('Streamers')
    @Post('create-streamer')
    async createStreamer(
        @Body() body: StreamerRegisterDto,
        @Res() response: Response
    ): Promise<any> {

        const saltOrRounds = 10;
        const hash = await bcrypt.hash(body.password, saltOrRounds);

        let user = new UserEntity
        user.email = body.email
        user.password = hash
        let resultUser: UserEntity

        try {
            resultUser = await this.usersService.crateUser(user)
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' })
        }

        let streamer = new StreamerEntity;
        streamer.bank = body.bank
        streamer.cc = body.cc
        streamer.dvCc = body.dv_cc
        streamer.agency = body.agency
        streamer.dvAgency = body.dv_agency
        streamer.channelName = body.channelName
        streamer.userId = resultUser.id

        try {
            const result = await this.streamersService.createStreamer(streamer)
            response
                .status(HttpStatus.OK)
                .send({ data: result, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }

    @ApiTags('Streamers')
    // @UseGuards(JwtAuthGuard)
    // @ApiBearerAuth('access-token')
    @Put('update-streamer')
    async updateClient(
        @Body() body: StreamerUpdateDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const streamerEntity = new StreamerEntity
            streamerEntity.channelName = body.channelName;
            streamerEntity.bank = body.bank;
            streamerEntity.agency = body.agency;
            streamerEntity.dvAgency = body.dvAgency;
            streamerEntity.cc = body.cc;
            streamerEntity.dvCc = body.dvCc;

            const client = await this.streamersService.updateStreamer(body.id, streamerEntity)

            response
                .status(HttpStatus.OK)
                .send({ client, message: 'OK' });

        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Streamers')
    // @UseGuards(JwtAuthGuard)
    // @ApiBearerAuth('access-token')
    @Delete('delete-client')
    async deleteClient(
        @Body() body: StreamerDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const clients = await this.streamersService.deleteStreamer(parseInt(body.id))

            response
                .status(HttpStatus.OK)
                .send({ clients, message: 'OK' });

        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error: "Cliente com doaçoes realizadas não podem ser excluidos", message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Streamers')
    @Get('get-all-streamers')
    async getStreamers(
        @Res() response: Response
    ): Promise<any> {
        try {
            const streamers = await this.streamersService.getAllStreamers()
            response
                .status(HttpStatus.OK)
                .send({ data: streamers, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }

    @ApiTags('Streamers')
    @Get('donations')
    async getDonations(
        @Query() query: StreamerDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const donations = await this.donationsService.getDonationsStreamer(parseInt(query.id))
            response
                .status(HttpStatus.OK)
                .send({ data: donations, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }

    @ApiTags('Streamers')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @Get('get-streamer')
    async getStreamer(
        @Request() req,
        @Res() response: Response
    ): Promise<any> {
        try {
            const user = await this.streamersService.getStreamer(req.user.userId)
            
            if (user) {
                user.user.password = ""
                response
                    .status(HttpStatus.OK)
                    .send({ user, message: 'OK' });
            } else {
                response
                    .status(HttpStatus.BAD_REQUEST)
                    .send({ error: "Client not found", message: 'BAD REQUEST' });
            }
        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }
}
