import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Irroba')
    .setDescription('API teste irroba<br/><br/>Para utilizar a rota de doações, é preciso fazer login na rota de auth/login')
    .setVersion('1.0.0')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  if (process.env.NODE_ENV !== 'production') {
    app.enableCors();
  }
  let port: number = process.env.NEST_ENV === 'dev' ? 5000 : 80;
  console.log(port);
  await app.listen(port);
}
bootstrap();
