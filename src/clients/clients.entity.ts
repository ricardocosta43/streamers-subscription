import { IsInt, IsString } from "class-validator";
import { DonationsEntity } from "src/donations/donations.entity";
import { UserEntity } from "src/users/user.entity";
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("clients")
export class ClientsEntity {
    @PrimaryGeneratedColumn()
    @IsInt()
    id: number;

    @Column({ name: "user_id" })
    @IsInt()
    userId: number;

    @Column({ length: 255 })
    @IsString()
    name: string;

    @OneToMany(() => DonationsEntity, donation => donation.client)
    donations: DonationsEntity[];

    @OneToOne(() => UserEntity, user => user.id)
    @JoinColumn({ name: "user_id" })
    user: UserEntity;
}