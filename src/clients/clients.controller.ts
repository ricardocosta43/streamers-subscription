import { Body, Controller, Get, HttpStatus, Post, Query, Res, UseGuards, Request, Put, Delete } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ClientsService } from 'src/clients/clients.service';
import { Response } from 'express';
import { ClientsEntity } from 'src/clients/clients.entity';
import { ClientDto, ClientRegisterDto, ClientUpdateDto } from 'src/clients/dto/clients-dto';
import { UsersService } from 'src/users/users.service';
import { UserEntity } from 'src/users/user.entity';
import * as bcrypt from 'bcrypt';
import { DonationsService } from 'src/donations/donations.service';
import { JwtAuthGuard } from 'src/users/guards/jwt-auth.guard';

@Controller('clients')
export class ClientsController {
    constructor(
        private readonly clientsService: ClientsService,
        private readonly usersService: UsersService,
        private readonly donationsService: DonationsService
    ) { }

    @ApiTags('Clients')
    @Post('create-client')
    async signup(
        @Body() body: ClientRegisterDto,
        @Res() response: Response
    ): Promise<any> {

        const saltOrRounds = 10;
        const hash = await bcrypt.hash(body.password, saltOrRounds);

        let user = new UserEntity
        user.email = body.email
        user.password = hash
        let resultUser: UserEntity

        try {
            resultUser = await this.usersService.crateUser(user)
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' })
        }

        let client = new ClientsEntity;
        client.name = body.name
        client.userId = resultUser.id

        try {
            const result = await this.clientsService.createClient(client)
            response
                .status(HttpStatus.OK)
                .send({ data: result, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }

    }

    @ApiTags('Clients')
    // @UseGuards(JwtAuthGuard)
    // @ApiBearerAuth('access-token')
    @Put('update-client')
    async updateClient(
        @Body() body: ClientUpdateDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const clientEntity = new ClientsEntity
            clientEntity.name = body.name;

            const client = await this.clientsService.updateClient(body.id, clientEntity)

            response
                .status(HttpStatus.OK)
                .send({ client, message: 'OK' });

        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Clients')
    // @UseGuards(JwtAuthGuard)
    // @ApiBearerAuth('access-token')
    @Delete('delete-client')
    async deleteClient(
        @Body() body: ClientDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const clients = await this.clientsService.deleteClient(parseInt(body.id))

            response
                .status(HttpStatus.OK)
                .send({ clients, message: 'OK' });

        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error: "Cliente com doaçoes realizadas não podem ser excluidos", message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Clients')
    // @UseGuards(JwtAuthGuard)
    // @ApiBearerAuth('access-token')
    @Get('get-all-clients')
    async getAllClients(
        @Request() req,
        @Res() response: Response
    ): Promise<any> {
        try {
            const clients = await this.clientsService.getAllClients()

            response
                .status(HttpStatus.OK)
                .send({ clients, message: 'OK' });

        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Clients')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @Get('get-client')
    async getClient(
        @Request() req,
        @Res() response: Response
    ): Promise<any> {
        try {
            const user = await this.clientsService.getClient(req.user.userId)

            if (user) {
                user.user.password = ""
                response
                    .status(HttpStatus.OK)
                    .send({ user, message: 'OK' });
            } else {
                response
                    .status(HttpStatus.BAD_REQUEST)
                    .send({ error: "Client not found", message: 'BAD REQUEST' });
            }
        } catch (error) {
            response
                .status(HttpStatus.BAD_REQUEST)
                .send({ error, message: 'BAD REQUEST' });
        }
    }

    @ApiTags('Clients')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('access-token')
    @Get('donations')
    async getDonations(
        @Query() query: ClientDto,
        @Res() response: Response
    ): Promise<any> {
        try {
            const donations = await this.donationsService.getDonationsClient(parseInt(query.id))
            response
                .status(HttpStatus.OK)
                .send({ data: donations, message: 'OK' });
        } catch (error) {
            response
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .send({ data: error, message: 'error' });
        }
    }
}
