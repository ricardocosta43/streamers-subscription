import { Inject, Injectable } from '@nestjs/common';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ClientsEntity } from './clients.entity';

@Injectable()
export class ClientsService {
    constructor(
        @Inject('CLIENTS_REPOSITORY')
        private clientsRepository: Repository<ClientsEntity>,
    ) { }

    async createClient(client: ClientsEntity): Promise<ClientsEntity> {
        return this.clientsRepository.save(client);
    }

    async updateClient(id: number, client: ClientsEntity): Promise<UpdateResult> {
        return this.clientsRepository.update(id, client);
    }

    async deleteClient(id: number): Promise<DeleteResult> {
        return this.clientsRepository.delete(id);
    }

    async getAllClients(): Promise<ClientsEntity[]> {
        return this.clientsRepository.find();
    }

    async getClient(userId: number): Promise<ClientsEntity> {
        return this.clientsRepository.findOne({
            where: {
                userId: userId
            },
            relations: [
                "user"
            ]
        });
    }
}
