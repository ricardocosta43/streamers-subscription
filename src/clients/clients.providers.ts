import { Connection } from "typeorm";
import { ClientsEntity } from "./clients.entity";

export const clientsProviders = [
    {
        provide: 'CLIENTS_REPOSITORY',
        useFactory: (connection: Connection) =>
            connection.getRepository(ClientsEntity),
        inject: ['DATABASE_CONNECTION'],
    },
];