import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsString } from "class-validator";

export class ClientRegisterDto {
    @ApiProperty({ description: "Email" })
    @IsNotEmpty()
    @IsString()
    email: string;

    @ApiProperty({ description: "Senha" })
    @IsNotEmpty()
    @IsString()
    password: string;

    @ApiProperty({ description: "Nome do usuário" })
    @IsNotEmpty()
    @IsString()
    name: string;
}

export class ClientDto {
    @ApiProperty({ description: "Id do Cliente" })
    @IsNotEmpty()
    @IsString()
    id: string;
}

export class ClientUpdateDto {
    @ApiProperty({ description: "Id do Cliente" })
    @IsNotEmpty()
    @IsInt()
    id: number;
    
    @ApiProperty({ description: "Nome do usuário" })
    @IsNotEmpty()
    @IsString()
    name: string;
}