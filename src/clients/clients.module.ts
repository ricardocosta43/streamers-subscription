import { Module } from '@nestjs/common';
import { ClientsService } from './clients.service';
import { ClientsController } from './clients.controller';
import { DatabaseModule } from 'src/database/database.module';
import { UsersModule } from 'src/users/users.module';
import { clientsProviders } from './clients.providers';
import { DonationsModule } from 'src/donations/donations.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    DonationsModule,
  ],
  providers: [
    ...clientsProviders,
    ClientsService
  ],
  controllers: [ClientsController]
})
export class ClientsModule { }
