import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StreamersModule } from './streamers/streamers.module';
import { UsersModule } from './users/users.module';
import { ClientsModule } from './clients/clients.module';
import { DonationsModule } from './donations/donations.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    StreamersModule,
    UsersModule,
    ClientsModule,
    DonationsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
